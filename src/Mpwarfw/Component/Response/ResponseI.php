<?php

namespace Mpwarfw\Component\Response;

interface ResponseI
{
    public function setResponse($result);
    public function send();
}
