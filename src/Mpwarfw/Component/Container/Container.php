<?php

namespace Mpwarfw\Component\Container;

use ArrayAccess;
use InvalidArgumentException;

class Container implements ArrayAccess
{

    protected $values = array();

    public function __construct(array $values = array())
    {
        $this->values = $values;
    }

    public function offsetSet($id, $value)
    {
        $this->values[$id] = $value;
    }

    public function offsetGet($id)
    {
        if (!array_key_exists($id, $this->values)) {
            throw new InvalidArgumentException(sprintf('Identifier "%s" is not defined.', $id));
        }
        $isFactory = is_object($this->values[$id]) && method_exists($this->values[$id], '__invoke');
        return $isFactory ? $this->values[$id]($this) : $this->values[$id];
    }

    public function offsetExists($id)
    {
        return array_key_exists($id, $this->values);
    }

    public function offsetUnset($id)
    {
        unset($this->values[$id]);
    }

    public static function share($callable)
    {
        if (!is_object($callable) || !method_exists($callable, '__invoke')) {
            throw new InvalidArgumentException('Service definition is not a Closure or invokable object.');
        }
        return function ($c) use ($callable) {
            static $object;
            if (null === $object) {
                $object = $callable($c);
            }
            return $object;
        };
    }

    public function keys()
    {
        return array_keys($this->values);
    }
}
