<?php

namespace Mpwarfw\Component\Database;

interface DatabaseConnectionI
{
    public function __construct($host,$dbname,$user,$password);
    public function execute($query,$arrayOfParams);
}
